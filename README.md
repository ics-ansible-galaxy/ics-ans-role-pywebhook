ics-ans-role-pywebhook
======================

Ansible role to install py-webhook.

Requirements
------------

- ansible >= 2.7
- molecule >= 2.19

Role Variables
--------------

```yaml
pywebhook_user: pywebhook
pywebhook_version: v0.4.0
pywebhook_conf_file: "{{ playbook_dir }}/config/py-webhook.yml"
pywebhook_pex_file: "https://artifactory.esss.lu.se/artifactory/swi-pkg/pex/py-webhook/{{ pywebhook_version }}/py-webhook"
pywebhook_cron_jobs: []
```

The _pywebhook_cron_jobs_ variable shall be set to something like the following:

```yaml
pywebhook_cron_jobs:
  - name: Install EPICS deployment webhook on bitbucket EEM project
    args: bitbucket -p FOO "EPICS deployment"
  - name: Install ICS Ansible Galaxy webhook on gitlab ics-ansible-galaxy namespace
    args: gitlab -n ics-ansible-galaxy "ICS Ansible Galaxy"
```

Note that the name of the webhook shall be defined in the .py-webhook.yml configuration.

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-pywebhook
```

License
-------

BSD 2-clause
