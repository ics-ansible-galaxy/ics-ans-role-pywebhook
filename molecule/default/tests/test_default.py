import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_run_pywebhook(host):
    cmd = host.run('/usr/local/bin/py-webhook --help')
    assert cmd.rc == 0
    assert 'Script to modify Bitbucket and Gitlab webhooks' in cmd.stdout


def test_pywebhook_conf_installed(host):
    conf = host.file('/home/pywebhook/.py-webhook.yml')
    assert conf.user == 'pywebhook'
    assert conf.mode == 0o600
