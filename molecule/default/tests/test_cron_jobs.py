import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('cron_jobs')


def test_pywebhook_crontab(host):
    crontab = host.check_output('crontab -u pywebhook -l')
    assert crontab == """#Ansible: Install EPICS deployment webhook on bitbucket EEM project
*/10 * * * * /usr/local/bin/py-webhook bitbucket -p FOO "EPICS deployment" > /tmp/py-webhook.0.log 2>&1
#Ansible: Install ICS Ansible Galaxy webhook on gitlab ics-ansible-galaxy namespace
*/10 * * * * /usr/local/bin/py-webhook gitlab -n ics-ansible-galaxy "ICS Ansible Galaxy" > /tmp/py-webhook.1.log 2>&1"""
